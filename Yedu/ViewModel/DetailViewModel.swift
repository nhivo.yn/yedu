//
//  DetailViewModel.swift
//  Yedu
//
//  Created by NhiVHY on 19/06/2022.
//

import Foundation

class DetailViewModel: NSObject {
    let network = NetworkService()
    var userDetail: UserResponse?
    var fetchDetailSuccess: () -> Void = { }
    var fetchDataFailure: (String) -> Void = { _ in }
    
}

extension DetailViewModel {
    func getUserDetail(id: String) {
        network.getUserById(id: id) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case.success(let userDetail):
                guard let userDetail = userDetail else { return }
                self.userDetail = userDetail
                self.fetchDetailSuccess()
            case .failure(let error):
                self.fetchDataFailure(error.localizedDescription)
            }
        }
    }
}
