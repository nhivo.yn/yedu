//
//  HomeViewModel.swift
//  Yedu
//
//  Created by NhiVHY on 19/06/2022.
//

import Foundation

class HomeViewModel: NSObject {
    let network = NetworkService()
    var listUser = [UserResponse]()
    var fetchListUserSuccess: () -> Void = { }
    var fetchDataFailure: (String) -> Void = { _ in }
}

extension HomeViewModel {
    func getListUser(page: Int, limit: Int) {
        network.getListUser(page: page, limit: limit) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case.success(let listUser):
                guard let listUser = listUser else { return }
                self.listUser = listUser.data
                self.fetchListUserSuccess()
            case .failure(let error):
                self.fetchDataFailure(error.localizedDescription)
            }
        }
    }
}
