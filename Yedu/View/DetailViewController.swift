//
//  DetailViewController.swift
//  Yedu
//
//  Created by NhiVHY on 19/06/2022.
//

import UIKit

class DetailViewController: UIViewController {

    // MARK: - IBOutlets
    @IBOutlet private weak var avatarImageView: UIImageView!
    @IBOutlet private weak var firstNameTextField: UITextField!
    @IBOutlet private weak var lastNameTextField: UITextField!
    @IBOutlet private weak var changeNameButton: UIButton!
    @IBOutlet private weak var deleteButton: UIButton!
    
    // MARK: - Properties
    var id: String = ""
    let viewModel = DetailViewModel()
    
    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        bindViewAndViewModel()
    }
    
    // MARK: - Bind view and view model
    func bindViewAndViewModel() {
        viewModel.fetchDetailSuccess = {
            self.firstNameTextField.text = self.viewModel.userDetail?.firstName ?? ""
            self.firstNameTextField.isEnabled = false
            self.lastNameTextField.text = self.viewModel.userDetail?.lastName ?? ""
            self.lastNameTextField.isEnabled = false
            if let url = URL(string: self.viewModel.userDetail?.picture ?? "") {
                self.avatarImageView.sd_setImage(with: url)
            }
        }
        
        viewModel.fetchDataFailure = { error in
            print(error)
        }
    }

}

// MARK: - Helpers
extension DetailViewController {
    private func setup() {
        viewModel.getUserDetail(id: id)
        changeNameButton.layer.cornerRadius = 8
        changeNameButton.layer.borderWidth = 0.2
        changeNameButton.layer.borderColor = UIColor.gray.cgColor
        changeNameButton.setTitle("Change name", for: .normal)
        changeNameButton.setTitle("Change name", for: .highlighted)
        changeNameButton.tintColor = .black

        deleteButton.layer.cornerRadius = 8
        deleteButton.setTitle("Delete", for: .normal)
        deleteButton.setTitle("Delete", for: .highlighted)
        deleteButton.tintColor = .black
        
        avatarImageView.layer.cornerRadius = avatarImageView.frame.size.height/2
    }
}

