//
//  HomeViewController.swift
//  Yedu
//
//  Created by NhiVHY on 18/06/2022.
//

import UIKit

class HomeViewController: UIViewController {

    // MARK: - IBOutlets
    @IBOutlet private weak var listUserTableView: UITableView!
    
    // MARK: - Properties
    private let viewModel = HomeViewModel()
    
    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        bindViewandViewModel()
    }
    
    // MARK: - Bind view and view model
    func bindViewandViewModel() {
        viewModel.fetchListUserSuccess = {
            self.listUserTableView.reloadData()
        }
        
        viewModel.fetchDataFailure = { error in
            print(error)
        }
    }
    
}

// MARK: - Helpers
extension HomeViewController {
    private func setup() {
        viewModel.getListUser(page: 1, limit: 10)
        listUserTableView.delegate = self
        listUserTableView.dataSource = self
        listUserTableView.registerReusedCell(cellNib: PreviewUserTableViewCell.self)
    }
}

// MARK: - UITableViewDelegate
extension HomeViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let detailViewController = DetailViewController()
        detailViewController.id = viewModel.listUser[indexPath.row].id
        self.navigationController?.pushViewController(detailViewController, animated: true)
    }
}

// MARK: - UITableViewDataSource
extension HomeViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.listUser.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusable(cellNib: PreviewUserTableViewCell.self)
        cell?.configure(data: viewModel.listUser[indexPath.row])
        return cell ?? UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
 
}
