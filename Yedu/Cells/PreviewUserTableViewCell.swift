//
//  PreviewUserTableViewCell.swift
//  Yedu
//
//  Created by NhiVHY on 19/06/2022.
//

import UIKit
import SDWebImage

class PreviewUserTableViewCell: UITableViewCell {
    @IBOutlet private weak var outsideView: UIView!
    @IBOutlet private weak var avatarImageView: UIImageView!
    @IBOutlet private weak var firstNameLabel: UILabel!
    @IBOutlet private weak var lastNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
}

// MARK: - Helpers
extension PreviewUserTableViewCell {
    func setup() {
        self.selectionStyle = .none
        avatarImageView.layer.cornerRadius = avatarImageView.frame.size.height/2
        outsideView.layer.cornerRadius = 8
        outsideView.layer.borderWidth = 0.2
        outsideView.layer.borderColor = UIColor.gray.cgColor
    }
    
    func configure(data: UserResponse) {
        firstNameLabel.text = data.firstName
        lastNameLabel.text = data.lastName
        if let url = URL(string: data.picture) {
            avatarImageView.sd_setImage(with: url, completed: nil)
        }
    }
}

