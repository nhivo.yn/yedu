//
//  ListUserResponse.swift
//  Yedu
//
//  Created by NhiVHY on 18/06/2022.
//

import Foundation

struct ListUserResponse: Codable {
    let total: Int
    let page: Int
    let limit: Int
    let data: [UserResponse]
}

struct UserResponse: Codable {
    let id: String
    let title: String
    let firstName: String
    let lastName: String
    let picture: String
}

struct DeleteUserResponse: Codable {
    let id: String
}

